Final implementation of the Task 5

This repostory joins all the respositories (group and individual) of this task in a common place.

Contains:

1.  Task5Interface: Image Operators Interface
2.  Task5TestImageOperators: the actual class library of the unit tests
3.  Task5TestProgram: a Windows Form project that run the tests and show results
4.  ImplementationSolution: a class library that contains the individual functions implementation of the interface
